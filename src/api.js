import store from "./store";

const axios = require("axios");

// hacer una solicitud para un usuario con una determinada id
function get(url, urlParams) {
  return (
    axios
      //llamada a la consulta
      .get("http://142.93.235.212/cursovuepeliculas" + url, {
        params: urlParams,
        headers: { Authorization: "Bearer " + store.state.token }
      })
      //las promesas son funciones que devuelven algo en algun momento. funciones asincronas
      .then(function(response) {
        // handle sucess
        return response;
      })
      .catch(function(error) {
        //Cuando se produce un error
        console.log(error);
      })
  );
}

//enviamos parametros para almacenar
function post(url, urlParams) {
  return axios
    .post("http://142.93.235.212/cursovuepeliculas" + url, urlParams, {
      headers: { Authorization: "Bearer " + store.state.token }
    })
    .then(function(response) {
      return response;
    })
    .catch(function(error) {
      console.log(error);
    });
}

export default {
  get,
  post
};
