import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

/*
state.usuario = "";

if (localStorage.getItem("usuario") == null) {
  state.usuario = null;
} else { 
  let stringUsuario = localStorage.getItem("usuario");
  state.usuario = JSON.parse(stringUsuario);
}

state.usuario =
  localStorage.getItem("usuario") != null
  ? JSON.parse(localStorage.getItem("usuario"))
  : null,
*/
export default new Vuex.Store({
  state: {
    usuario:
      localStorage.getItem("usuario") != null
        ? JSON.parse(localStorage.getItem("usuario"))
        : null,
    token:
      localStorage.getItem("token") != null
        ? localStorage.getItem("token")
        : null
  },

  mutations: {
    incrementar(state) {
      state.contador = state.contador + 1;
    },

    login(state, datosUsuarios) {
      state.usuario = datosUsuarios.usuario;
      state.token = datosUsuarios.token;

      localStorage.setItem("usuario", JSON.stringify(state.usuario));
      localStorage.setItem("token", state.token);
    },
    logout(state) {
      state.usuario = null;
      state.token = null;

      localStorage.removeItem("usuario");
      localStorage.removeItem("token");
    }
  },

  getters: {},
  actions: {},
  modules: {}
});
