import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/home/Home.vue";
import Usuarios from "../views/usuarios/Usuarios.vue";
import Tecnologia from "../views/tecnologia/Tecnologia.vue";
import Entretenimiento from "../views/entretenimiento/Entretenimiento.vue";
import Login from "../views/login/login.vue";
import Peliculas from "../views/noticias/Peliculas.vue";
import Generos from "../views/generos/Generos.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/generos",
    name: "generos",
    component: Generos
  },
  {
    path: "/peliculas",
    name: "peliculas",
    component: Peliculas
  },
  {
    path: "/usuarios",
    name: "Usuarios",
    component: Usuarios
  },
  {
    path: "/categorias/tecnologia",
    name: "Tecnologia",
    component: Tecnologia
  },
  {
    path: "/categorias/entretenimiento",
    name: "Entretenimiento",
    component: Entretenimiento
  },
  {
    path: "/login",
    name: "Login",
    component: Login
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
