import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";

import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import locale from "element-ui/lib/locale/lang/en";

Vue.config.productionTip = false;
Vue.use(ElementUI, { locale });

//Importar la api
import api from "./api";
Vue.prototype.$api = api;

require("./assets/fuente.css");

//////////  Verificar si esta logueado antes de ir las paginas ////////
router.beforeEach((to, from, next) => {
  let requiereLogin = false;

  if (to.name != "Login") {
    //Login es el nombre de la pagina asignada en index de Router
    requiereLogin = true;
  }

  if (requiereLogin && store.state.usuario == null) {
    router.push("/login");
  } else {
    if (!requiereLogin && store.state.usuario != null) {
      router.push("/");
    }
    next(); //continua normalmente
  }
});

////////// Fin Verificacion ///////////////
new Vue({
  el: "#app",
  router,
  store,
  render: h => h(App)
}).$mount("#app");
